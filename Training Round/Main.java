import java.io.*;
import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static int m=0,max;
    public static ArrayList<Integer> listaIndici,listaPizze=new ArrayList<>();
    public static int  pos_numero=1;

    public static ArrayList<Integer> calcola(int numero, int i,ArrayList<Integer> indici) throws FileAlreadyExistsException {
        int indice=i;
        while(indice<listaPizze.size()-pos_numero && numero+listaPizze.get(indice)<=max) {
            int n = numero + listaPizze.get(indice);
            if (n == max) {
                indici.add(indice);
                m=n;
                listaIndici= (ArrayList<Integer>) indici.clone();
                throw new FileAlreadyExistsException("");
            } else {
                if (n < max) {
                    indici.add(indice);
                    calcola(n, indice+1,indici);
                    if(n>m){
                        m=n;
                        listaIndici= (ArrayList<Integer>) indici.clone();
                    }
                    indici.remove(indici.size()-1);
                }
            }
            indice++;
        }
        return null;
    }

    public static void main(String[] args) throws IOException {
        String path ="C:\\Users\\alberto\\Desktop\\a_example.in";
        File file = new File(path);
        File out = new File(path+"Out.txt");
        BufferedReader br = new BufferedReader(new FileReader(file));
        String[] npezzi_pizze=br.readLine().split(" ");
        max=Integer.valueOf(npezzi_pizze[0]);
        String st,s="";
        while ((st = br.readLine()) != null)
            s+=st;
        ArrayList<Integer> risultato,indici;
        Arrays.stream(s.split(" ")).forEach(x->listaPizze.add(Integer.valueOf(x)));

        try {
            for (int i = listaPizze.size() - 1; i > (listaPizze.size() / 3)*2; i--) {
                risultato = new ArrayList<>();
                indici = new ArrayList<>();
                risultato.add(listaPizze.get(listaPizze.size() - pos_numero));
                indici.add(listaPizze.size()-pos_numero);

                calcola(listaPizze.get(i), 0,indici);
                pos_numero++;
            }
        }
        catch (FileAlreadyExistsException e){}
        String sout="";
        listaIndici.add(listaIndici.get(0));
        listaIndici.remove(0);
        for(int ind:listaIndici){ sout=sout+""+ind+" "; }
        out.createNewFile();
        BufferedWriter bw=new BufferedWriter(new FileWriter(out));
        bw.write(Integer.toString(listaIndici.size())+"\n");
        bw.write(sout);
        bw.flush();
        bw.close();

    }

}
