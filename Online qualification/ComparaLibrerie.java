import java.util.Comparator;

public class ComparaLibrerie implements Comparator<Libreria> {

    @Override
    public int compare(Libreria p1, Libreria p2) {
        if (p1.getPriorità()-(p2.getPriorità ()) > 0) {
            return -1;
        } else if (p1.getPriorità()-(p2.getPriorità ()) < 0) {
            return 1;
        }
        return 0;
    }

}

