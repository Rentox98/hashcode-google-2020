import java.util.Comparator;

public class CompareBook implements Comparator<Book> {
    @Override
    public int compare(Book p1, Book p2) {
        if (p1.getValore()-(p2.getValore ()) > 0) {
            return -1;
        } else if (p1.getValore()-(p2.getValore ()) < 0) {
            return 1;
        }
        return 0;
    }
}
