import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Libreria {

    private int num_libri,giorni_reg,scan_day;
    private int id;
    private double priorità;
    private boolean isSign=false;
    private int indice_scan=0;
    private ArrayList<Book> libri=new ArrayList<>();
    private ArrayList<Book> libri_scan=new ArrayList<>();


    public Libreria(String[] a, int i){
        num_libri=Integer.valueOf(a[0]);
        giorni_reg=Integer.valueOf(a[1]);
        scan_day=Integer.valueOf(a[2]);
        id=i;
    }


    public void addBook(Book b){
        libri.add(b);
    }

    public void calc_priorità(){
        for(Book b:libri)
            priorità+=b.getValore();
        priorità=priorità/(libri.size()+scan_day);

    }

     public void setSign(){
        isSign=true;
     }

    public double getPriorità() {
        return priorità;
    }

    public void ordinaLibir(){
        Collections.sort(libri,new CompareBook());
    }

    public int getGiorniReg() {
        return giorni_reg;
    }

    public int getMaxScanDay() {
        return scan_day;
    }

    public int getNumLibri() {
        return num_libri;
    }

    public void scanBook() {
        libri_scan.add(libri.get(indice_scan++));
        num_libri--;
    }

    public boolean getSign() {
        return isSign;
    }

    public int getId() {
        return id;
    }

    public ArrayList<Book> getLibriScann() {
        return libri_scan;
    }
}
