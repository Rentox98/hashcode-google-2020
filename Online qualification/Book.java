public class Book {
    private int valore,indice;

    public Book(int v,int i){
        valore=v;
        indice=i;
    }

    public int getValore() {
        return valore;
    }

    public int getIndice() {
        return indice;
    }
}
