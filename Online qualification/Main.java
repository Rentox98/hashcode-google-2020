import java.io.*;
import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class Main {

    public static void main(String[] args) throws IOException {

        String path ="C:\\Users\\alberto\\Desktop\\a_example.txt";
        File file = new File(path);
        File out = new File(path+"Out.txt");
        BufferedReader br = new BufferedReader(new FileReader(file));
        String[] lib_libr_day=br.readLine().split(" ");
        String[] score_books=br.readLine().split(" ");
        int[] punteggi=new int[score_books.length];
        int i=0;
        for(String s:score_books){
            punteggi[i]=Integer.valueOf(s);
            i++;
        }
        int num_libri,num_librerie,num_giorni;
        num_libri=Integer.valueOf(lib_libr_day[0]);
        num_librerie=Integer.valueOf(lib_libr_day[1]);
        num_giorni=Integer.valueOf(lib_libr_day[2]);
        String s;
        int pt;
        int indice;
        Book b;
        ArrayList<Libreria> librerie= new ArrayList<>();
        int indice_libreria=0;
        while((s=br.readLine())!=null){
            Libreria l=new Libreria(s.split(" "),indice_libreria);
            librerie.add(l);
            indice_libreria++;
            for(String bb:br.readLine().split(" ")){
                indice=Integer.valueOf(bb);
                pt=punteggi[indice];
                l.addBook(new Book(pt,indice));
            }
            l.calc_priorità();
        }
        Collections.sort(librerie,new ComparaLibrerie());
        ArrayList<Libreria> librerie_usate=new ArrayList<>();
        int c=0;
        Libreria lib;
        while(num_giorni!=0){
            if(c<librerie.size()){
                lib=librerie.get(c);
                librerie_usate.add(lib);
                num_giorni-=lib.getGiorniReg();
                lib.setSign();
                c++;}
            else
                break;
            for(int in=0;in<=c-1;in++){
  //              if(librerie.get(in).getSign())
                    if(librerie.get(in).getNumLibri()!=0)
                        for(int il=0;il<librerie.get(in).getMaxScanDay();il++){
                            if(librerie.get(in).getNumLibri()!=0)
                                librerie.get(in).scanBook();
                        }
            }

        }


        librerie.stream().forEach(x->System.out.println(x.getPriorità()));
        ArrayList<Book> libriscann=new ArrayList<>();
        String sout="";
        out.createNewFile();
        BufferedWriter bw=new BufferedWriter(new FileWriter(out));
        bw.write(librerie_usate.size()+"\n");
        for(Libreria l:librerie_usate){
            libriscann=l.getLibriScann();
            bw.write(l.getId()+" "+libriscann.size()+"\n");
            for(Book book:libriscann){
                sout=sout+""+book.getIndice()+" ";
            }
            bw.write(sout+"\n");
            sout="";
        }

        bw.flush();
        bw.close();
    }


}
